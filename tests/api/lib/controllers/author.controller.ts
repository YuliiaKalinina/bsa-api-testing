import { ApiRequest } from "../request";

let baseUrl: string = "https://knewless.tk/api/";

export class AuthorController {
  async authorSetSettings(accessToken: string, authorSettings: object) {
    const response = await new ApiRequest()
      .prefixUrl(baseUrl)
      .method("POST")
      .bearerToken(accessToken)
      .body(authorSettings)
      .url(`author`)
      .send();
    return response;
  }
}