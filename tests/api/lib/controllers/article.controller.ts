import { ApiRequest } from "../request";

let baseUrl: string = "https://knewless.tk/api/";

export class ArticleController {
  async getArticleInfo(accessToken) {
    const response = await new ApiRequest()
      .prefixUrl(baseUrl)
      .method("GET")
      .url(`article/author`)
      .bearerToken(accessToken)
      .send();
    return response;
  }
}