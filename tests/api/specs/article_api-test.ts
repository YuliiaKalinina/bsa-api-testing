import { expect } from 'chai';
import { checkStatusCode } from '../../helpers/functionsForChecking.helper';
import { AuthController } from '../lib/controllers/auth.controller';
const auth = new AuthController();
import { UserController } from '../lib/controllers/user.controller';
const userInfo = new UserController();
import { ArticleController } from '../lib/controllers/article.controller';
const articles = new ArticleController();

xdescribe('Articles controller', () => {
  let courseId: string;
  let accessToken: string;
  let articleCount: number;
  let userId: string;
  let email: string;
  let password: string;

  it(`loginUser`, async () => {
    let response = await auth.authUser('yuliia.kalinina0022@gmail.com', 'realPlanning 1000');
    checkStatusCode(response, 200);
    accessToken = response.body.accessToken;
  });

  it(`getUserInfo`, async () => {
    let response = await userInfo.getUserInfo(accessToken);
    checkStatusCode(response, 200);
    userId = response.body.id;
  });

  it(`getArticleInfo`, async () => {
    let response = await articles.getArticleInfo(accessToken);
    checkStatusCode(response, 200);
    articleCount = response.body.length;
  });
});
