import { expect } from 'chai';
import { checkStatusCode, checkResponseTime } from '../../helpers/functionsForChecking.helper';
import { CoursesController } from '../lib/controllers/courses.controller';
const courses = new CoursesController();
const schemas = require('./data/schemas_testData.json');
const chai = require('chai');
chai.use(require('chai-json-schema'));

describe('Courses controller', () => {
  let courseId: string;

  it(`getAllCourses`, async () => {

    let response = await courses.getAllCourses();

    checkStatusCode(response, 200);
    checkResponseTime(response, 5000);
    expect(response.body.length).to.be.greaterThan(1);

    courseId = response.body[10].id;
  });

  it(`getPopularCourses`, async () => {
    let response = await courses.getPopularCourses();

    checkStatusCode(response, 200);
    checkResponseTime(response, 5000);
    expect(response.body.length).to.be.greaterThan(1);
  });

  it(`getCourseDetails`, async () => {
    let response = await courses.getAllCourseInfoById(courseId);

    checkStatusCode(response, 200);
    checkResponseTime(response, 5000);
    expect(response.body).to.be.jsonSchema(schemas.schema_courseInfo);
  });
})