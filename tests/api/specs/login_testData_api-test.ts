import {
  checkStatusCode,
  checkResponseBodyStatus,
  checkResponseBodyMessage,
  checkResponseTime
} from '../../helpers/functionsForChecking.helper';
import { AuthController } from '../lib/controllers/auth.controller';
const auth = new AuthController();

describe('Use test data', () => {
  let invalidCredentialsDataSet = [
    { email: 'yuliia.kalinina0022@gmail.com', password: '                ' },
    { email: 'yuliia.kalinina0022@gmail.com', password: ' realPlanning1000' },
    { email: 'yuliia.kalinina0022@gmail.com', password: 'realPlanning 1000' },
    { email: 'yuliia.kalinina0022@gmail.com', password: 'author' },
    { email: 'yuliia.kalinina0022@gmail.com', password: 'yuliia.kalinina0022@gmail.com' },
    { email: 'yuliia.kalinina0022 @gmail.com', password: 'realPlanning1000' },
    { email: 'yuliia.kalinina0022gmail.com', password: 'realPlanning 1000' },
    { email: 'yuliia.kalinina0022@gmail.com ', password: 'realPlanning 1000' },
  ];
  invalidCredentialsDataSet.forEach((credentials) => {
    it(`login using invalid credentials : ${credentials.email} + ${credentials.password}`, async () => {
      let response = await auth.authUser(credentials.email, credentials.password);

      checkStatusCode(response, 401);
      checkResponseBodyStatus(response, 'UNAUTHORIZED');
      checkResponseBodyMessage(response, 'Bad credentials');
      checkResponseTime(response, 3000);
    });
  });
});